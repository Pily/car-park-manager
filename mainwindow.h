#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>

#include "aggiungimezzo.h"
#include "cambiaposti.h"
#include "aboutdialog.h"
#include "messagedialog.h"
#include "excepitons.h"
#include "pagamentodialog.h"
#include "eliminazionedialog.h"
#include "resetdialog.h"
#include "controller.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        explicit MainWindow( Controller *const _mainController, QWidget *parent = 0 );
        ~MainWindow();

        void UpdateDataView();

    private slots:
        void on_actionChiudi_triggered();

        void on_pushButton_Aggiungi_clicked();

        void on_actionPosti_triggered();

        void on_actionAbout_triggered();

        void on_pushButton_Togli_clicked();

        void on_pushButton_Paga_clicked();

        void on_actionCosti_triggered();

        void on_actionNuovo_triggered();

        void on_actionCarica_triggered();

        void on_actionSalva_triggered();

private:
        Ui::MainWindow *ui;
        Controller *const PTMainController;
};

#endif // MAINWINDOW_H
