#ifndef PAGAMENTODIALOG_H
#define PAGAMENTODIALOG_H

#include <QDialog>
#include "controller.h"

namespace Ui
{
    class pagamentoDialog;
}

class pagamentoDialog : public QDialog
{
        Q_OBJECT

    public:
        explicit pagamentoDialog( DLista<MezzoParcheggiato>::Iterator _iter, Controller * const _PTMainController , QWidget *parent = 0 );
        ~pagamentoDialog();

private slots:

    void on_dateTimeEdit_dateTimeChanged(const QDateTime &dateTime);

    void on_pushButton_ok_clicked();

    void on_pushButton_cancel_clicked();

private:
        Ui::pagamentoDialog *ui;
        DLista<MezzoParcheggiato>::Iterator iter;
        Controller * const PTMainController;
};

#endif // PAGAMENTODIALOG_H
