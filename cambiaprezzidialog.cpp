#include "cambiaprezzidialog.h"
#include "ui_cambiaprezzidialog.h"
#include "excepitons.h"
#include "messagedialog.h"

cambiaPrezziDialog::cambiaPrezziDialog( Controller *const _PTMainController, QWidget *parent ) :
    QDialog( parent ),
    ui( new Ui::cambiaPrezziDialog ),
    PTMainController( _PTMainController )

{
    ui->setupUi( this );

    ui->doubleSpinBox_Camion->setValue( PTMainController->GetCostoCamion() );
    ui->doubleSpinBox_Camper->setValue( PTMainController->GetCostoCamper() );
    ui->doubleSpinBox_Macchine->setValue( PTMainController->GetCostoMacchine() );
    ui->doubleSpinBox_Moto->setValue( PTMainController->GetCostoMoto() );
}

cambiaPrezziDialog::~cambiaPrezziDialog()
{
    delete ui;
}

void cambiaPrezziDialog::on_pushButton_ok_clicked() try
{
    PTMainController->SetCostoCamion( ui->doubleSpinBox_Camion->value() );
    PTMainController->SetCostoCamper( ui->doubleSpinBox_Camper->value() );
    PTMainController->SetCostoMacchina( ui->doubleSpinBox_Macchine->value() );
    PTMainController->SetCostoMoto( ui->doubleSpinBox_Moto->value() );
    close();

} catch ( const EXCEPTIONS &e )
{
    if ( e == exception_ValoreNonValido ) {
        MessageDialog errore( "Valore inserito non valido",
                              "Si prega di controllare i valori inseriti. I valori negativi non sono accettati." );
        errore.exec();
    }
}

void cambiaPrezziDialog::on_pushButton_annulla_clicked()
{
    close();
}
