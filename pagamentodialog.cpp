#include "pagamentodialog.h"
#include "ui_pagamentodialog.h"

pagamentoDialog::pagamentoDialog( DLista<MezzoParcheggiato>::Iterator _iter, Controller *const _PTMainController, QWidget *parent ) :
    QDialog( parent ),
    ui( new Ui::pagamentoDialog ),
    iter( _iter ),
    PTMainController( _PTMainController )
{
    ui->setupUi( this );

    ui->label_arrivo_value->setText( ( *iter ).GetArrivo().toString() );
    ui->label_targa_value->setText( ( *iter ).GetTarga() );
    ui->dateTimeEdit->setDateTime( QDateTime::currentDateTime() );
    ui->dateTimeEdit->setMinimumDateTime( ( *iter ).GetArrivo() );
    ui->label_costo_value->setText( QString::number( ( *iter ).GetCostoServizio( ui->dateTimeEdit->dateTime() ) ) );
}

pagamentoDialog::~pagamentoDialog()
{
    delete ui;
}

void pagamentoDialog::on_dateTimeEdit_dateTimeChanged( const QDateTime &dateTime )
{
    ui->label_costo_value->setText( QString::number( ( *iter ).GetCostoServizio( dateTime ) ) );
}

void pagamentoDialog::on_pushButton_ok_clicked()
{
    PTMainController->TogliMezzo( iter );
    close();
}

void pagamentoDialog::on_pushButton_cancel_clicked()
{
    close();
}
