#ifndef EXCEPITONS_H
#define EXCEPITONS_H

enum EXCEPTIONS {
    exception_MezzoNonTrovato,
    exception_TypeNotSupported,
    exception_PostiEsauriti,
    exception_TargaNonValida,
    exception_MezziAncoraInLista,
    exception_NoItemSelected,
    exception_OperazioneAnnullata,
    exception_ValoreNonValido,
    exception_FileNonTrovato
};

#endif // EXCEPITONS_H
