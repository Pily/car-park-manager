#ifndef CAMBIAPOSTI_H
#define CAMBIAPOSTI_H

#include <QDialog>
#include "controller.h"

namespace Ui {
class CambiaPosti;
}

class CambiaPosti : public QDialog
{
    Q_OBJECT

public:
    explicit CambiaPosti(Controller * const _mainController ,QWidget *parent = 0);
    ~CambiaPosti();

private slots:
    void on_pushButton_annulla_clicked();

    void on_pushButton_ok_clicked();

private:
    Ui::CambiaPosti *ui;
    Controller * const PTMainController;
};

#endif // CAMBIAPOSTI_H
