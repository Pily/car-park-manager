#ifndef AUTOMEZZO_H
#define AUTOMEZZO_H

#include <QString>
#include <QDebug>

class AutoMezzo
{
        QString targa;
    public:

        enum Exceptions {
            exception_PostiOccupati_asbsurd,
            exception_CostoPerOra_absurd
        };

        AutoMezzo( const QString &_targa );
        virtual ~AutoMezzo();

        virtual double GetCostoServizioPerOra() const = 0;
        virtual unsigned int GetPostiOccupati() const;
        QString GetTarga() const;

        bool operator< ( const AutoMezzo &x ) const;
        bool operator> ( const AutoMezzo &x ) const;

        bool operator!= ( const AutoMezzo &x ) const;
        bool operator== ( const AutoMezzo &x ) const;

        friend QDebug operator<< ( QDebug, AutoMezzo *const );
};

#endif // AUTOMEZZO_H
