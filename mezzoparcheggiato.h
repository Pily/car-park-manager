#ifndef MEZZOPARCHEGGIATO_H
#define MEZZOPARCHEGGIATO_H

#include "mezzi.h"
#include <QDateTime>

class MezzoParcheggiato
{
        AutoMezzo *const mezzo;
        QDateTime arrivo;
    public:

        MezzoParcheggiato( AutoMezzo *const _mezzo, const QDateTime &_arrivo );
        MezzoParcheggiato( const QDateTime &_arrivo, const Camion &_mezzo, unsigned int _postiOccupati );

        QString GetTarga() const;
        double GetCostoPerOra() const;
        unsigned int GetPostiOccupati() const;
        QDateTime GetArrivo() const;
        double GetCostoServizio( const QDateTime &_uscita );
        AutoMezzo *GetMezzo() const;

        bool operator !=( const MezzoParcheggiato & ) const;
        bool operator ==( const MezzoParcheggiato & ) const;
        bool operator <( const MezzoParcheggiato & ) const;
        bool operator >( const MezzoParcheggiato & ) const;
};

#endif // MEZZOPARCHEGGIATO_H
