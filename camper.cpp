#include "camper.h"

double Camper::costoPerOra = 2.5;

Camper::Camper( const QString &_targa ) : AutoMezzo( _targa ) {}

Camper::~Camper() {}

double Camper::GetCostoServizioPerOra() const
{
    return costoPerOra;
}

double Camper::GetCostoPerOra()
{
    return costoPerOra;
}

double Camper::SetCostoPerOra( const double &_CostoPerOra ) try
{
    if ( _CostoPerOra < 0 ) {
        throw ( exception_CostoPerOra_absurd );
    }

    return costoPerOra = _CostoPerOra;

} catch ( const Exceptions &e )
{
    if ( e == exception_CostoPerOra_absurd ) {
        qDebug() << "Costo per ora assurdo: " << _CostoPerOra;
    }

    return costoPerOra;
}
