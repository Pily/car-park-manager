#include "resetdialog.h"
#include "ui_resetdialog.h"

resetDialog::resetDialog( Controller *const _PTMainController, QWidget *parent ) :
    QDialog( parent ),
    ui( new Ui::resetDialog ),
    PTMainController( _PTMainController )
{
    ui->setupUi( this );
}

resetDialog::~resetDialog()
{
    delete ui;
}

void resetDialog::on_pushButton_ok_clicked()
{
    PTMainController->Reset();
    close();
}

void resetDialog::on_pushButton_cancel_clicked()
{
    close();
}
