#include "macchina.h"

double Macchina::costoPerOra = 1;

Macchina::Macchina( const QString &_targa ) : AutoMezzo( _targa ) {}

Macchina::~Macchina() {}

double Macchina::GetCostoServizioPerOra() const
{
    return costoPerOra;
}

double Macchina::GetCostoPerOra()
{
    return costoPerOra;
}

double Macchina::SetCostoPerOra( const double &_CostoPerOra ) try
{
    if ( _CostoPerOra < 0 ) {
        throw ( exception_CostoPerOra_absurd );
    }

    return costoPerOra = _CostoPerOra;

} catch ( const Exceptions &e )
{
    if ( e == exception_CostoPerOra_absurd ) {
        qDebug() << "Costo per ora assurdo: " << _CostoPerOra;
    }

    return costoPerOra;
}
