#ifndef RESETDIALOG_H
#define RESETDIALOG_H

#include <QDialog>
#include "controller.h"

namespace Ui {
class resetDialog;
}

class resetDialog : public QDialog
{
    Q_OBJECT

public:
    explicit resetDialog(Controller * const _PTMainController, QWidget *parent = 0);
    ~resetDialog();

private slots:
    void on_pushButton_ok_clicked();

    void on_pushButton_cancel_clicked();

private:
    Ui::resetDialog *ui;
    Controller * const PTMainController;
};

#endif // RESETDIALOG_H
