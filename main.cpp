#include "mainwindow.h"
#include <QApplication>

#include "controller.h"

int main( int argc, char *argv[] )
{

    Controller mainController;

    QApplication a( argc, argv );
    MainWindow w( &mainController );
    w.show();

    return a.exec();
}
