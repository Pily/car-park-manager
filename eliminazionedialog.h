#ifndef ELIMINAZIONEDIALOG_H
#define ELIMINAZIONEDIALOG_H

#include <QDialog>
#include "controller.h"

namespace Ui
{
    class eliminazioneDialog;
}

class EliminazioneDialog : public QDialog
{
        Q_OBJECT

    public:
        explicit EliminazioneDialog( DLista<MezzoParcheggiato>::Iterator _iter, Controller *const _PTMainController, QWidget *parent = 0 );
        ~EliminazioneDialog();

    private slots:
        void on_pushButton_ok_clicked();

        void on_pushButton_cancel_clicked();

    private:
        Ui::eliminazioneDialog *ui;
        DLista<MezzoParcheggiato>::Iterator iter;
        Controller *const PTMainController;
};

#endif // ELIMINAZIONEDIALOG_H
