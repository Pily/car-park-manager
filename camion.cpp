#include "camion.h"

unsigned int Camion::dimensioniStandardCamion = 1;
double Camion::costoPerOra = 1.5;

Camion::Camion( const QString &_targa , const int &_postiOccupati ) : AutoMezzo( _targa ), postiOccupati( _postiOccupati ) {}

Camion::~Camion() {}

int Camion::SetPostiOccupati( const int &_postiOccupati ) try
{
    if ( _postiOccupati <= 0 ) {
        throw ( exception_PostiOccupati_asbsurd );
    }

    return postiOccupati = _postiOccupati;

} catch ( const Exceptions &e )
{

    if ( e == exception_PostiOccupati_asbsurd ) {
        qDebug() << "Posti occupati assurdi: " << _postiOccupati;
    } else {
        throw;
    }

    return postiOccupati;
}

unsigned int Camion::GetPostiOccupati() const
{
    return postiOccupati;
}

double Camion::GetCostoServizioPerOra() const
{
    return Camion::costoPerOra * postiOccupati;
}

unsigned int Camion::GetDimensioniStandardCamion()
{
    return dimensioniStandardCamion;
}

unsigned int Camion::SetDimensioniStandardCamion(unsigned int _dim) try
{
    if(_dim < 1){
        throw(exception_PostiOccupati_asbsurd);
    }

    return dimensioniStandardCamion = _dim;
} catch(const Exceptions& e){
    if(e == exception_PostiOccupati_asbsurd){
         qDebug() << "Posti occupati assurdi: " << _dim;
         return dimensioniStandardCamion;
    }
    else {
        throw;
    }
}

double Camion::GetCostoPerOra()
{
    return costoPerOra;
}

double Camion::SetCostoPerOra( const double &_CostoPerOra ) try
{
    if ( _CostoPerOra < 0 ) {
        throw ( exception_CostoPerOra_absurd );
    }

    return costoPerOra = _CostoPerOra;

} catch ( const Exceptions &e )
{
    if ( e == exception_CostoPerOra_absurd ) {
        qDebug() << "Costo per ora assurdo: " << _CostoPerOra;
    }

    return costoPerOra;
}
