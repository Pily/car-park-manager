#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "cambiaprezzidialog.h"

#include <QMessageBox>

MainWindow::MainWindow( Controller *const _mainController, QWidget *parent ) :
    QMainWindow( parent ), ui( new Ui::MainWindow ), PTMainController( _mainController )
{
    ui->setupUi( this );

    UpdateDataView();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::UpdateDataView()
{
    //Label tutto
    ui->label_Camion_value->setText( QString::number( PTMainController->GetSpaziCamionOccupati() ) + "/" + QString::number( PTMainController->GetSpaziCamion() ) ) ;
    ui->label_Camper_value->setText( QString::number( PTMainController->GetSpaziCamperOccupati() ) + "/" + QString::number( PTMainController->GetSpaziCamper() ) );
    ui->label_Macchine_value->setText( QString::number( PTMainController->GetSpaziMacchineOccupati() ) + "/" + QString::number( PTMainController->GetSpaziMacchine() ) );
    ui->label_Moto_value->setText( QString::number( PTMainController->GetSpaziMotoOccupati() ) + "/" + QString::number( PTMainController->GetSpaziMoto() ) );

    //Lista tutto
    ui->listWidget->clear();
    ui->listWidget->addItems( PTMainController->ToQStringList() );
}

void MainWindow::on_actionChiudi_triggered()
{
    close();
}

void MainWindow::on_pushButton_Aggiungi_clicked()
{
    AggiungiMezzo aggiungiMezzoDialog( PTMainController );
    aggiungiMezzoDialog.exec();
    UpdateDataView();
}

void MainWindow::on_actionPosti_triggered()
{
    CambiaPosti cambiaPostiDialog( PTMainController );
    cambiaPostiDialog.exec();
    UpdateDataView();
}

void MainWindow::on_actionAbout_triggered()
{
    AboutDialog aboutDialog;
    aboutDialog.exec();
}

void MainWindow::on_pushButton_Togli_clicked() try
{
    if ( ui->listWidget->selectedItems().size() == 0 ) {
        throw exception_NoItemSelected;
    }

    DLista<MezzoParcheggiato>::Iterator iter = PTMainController->CercaPerIndice( ui->listWidget->currentRow() );

    EliminazioneDialog eliminazione( iter, PTMainController, this );
    eliminazione.exec();

    UpdateDataView();

} catch ( const EXCEPTIONS &e )
{
    if ( e == exception_NoItemSelected ) {
        MessageDialog erroreTarga( "Nessun oggetto selezionato", "Per eseguire l'operazione deve essere selezionato un oggetto.", this );
        erroreTarga.exec();
    }
}

void MainWindow::on_pushButton_Paga_clicked() try
{
    if ( ui->listWidget->selectedItems().size() == 0 ) {
        throw exception_NoItemSelected;
    }

    DLista<MezzoParcheggiato>::Iterator iter = PTMainController->CercaPerIndice( ui->listWidget->currentRow() );

    pagamentoDialog pagamento( iter, PTMainController, this );
    pagamento.exec();

    UpdateDataView();

} catch ( const EXCEPTIONS &e )
{
    if ( e == exception_NoItemSelected ) {
        MessageDialog erroreTarga( "Nessun oggetto selezionato", "Per eseguire l'operazione deve essere selezionato un oggetto." );
        erroreTarga.exec();
    } else if ( e == exception_OperazioneAnnullata ) {
        //Catch vuota TEST
    }
}

void MainWindow::on_actionCosti_triggered()
{
    cambiaPrezziDialog cambiaPrezziDialog( PTMainController );
    cambiaPrezziDialog.exec();
    UpdateDataView();
}

void MainWindow::on_actionNuovo_triggered()
{
    resetDialog reset( PTMainController );
    reset.exec();
    UpdateDataView();
}

void MainWindow::on_actionCarica_triggered()try
{
    QString nomeFile = QFileDialog::getOpenFileName( this,
                       "Apri file dai", "./", "File DAT (*.dat)" );
    PTMainController->Load( nomeFile );
    UpdateDataView();

} catch ( const EXCEPTIONS &e )
{
    if(e == exception_ValoreNonValido){
        //non fare nulla
    } else if (e == exception_FileNonTrovato) {
        QString nomeFile = QFileDialog::getOpenFileName( this,
                           "Apri file dai", "./", "File DAT (*.dat)" );
        PTMainController->Load( nomeFile );
        UpdateDataView();
    }
    else {
        throw;
    }
}

void MainWindow::on_actionSalva_triggered() try
{
    QString nomeFile = QFileDialog::getSaveFileName( this,
                       "Salva file dati", "./", "File DAT (*.dat)" );
    PTMainController->Save( nomeFile );
    UpdateDataView();

} catch ( const EXCEPTIONS &e )
{
    if(e == exception_ValoreNonValido){
        //non fare nulla
    } else {
        throw;
    }
}
