#include "aggiungimezzo.h"
#include "ui_aggiungimezzo.h"
#include "mezzoparcheggiato.h"
#include "messagedialog.h"
#include "excepitons.h"

AggiungiMezzo::AggiungiMezzo( Controller *const _mainController, QWidget *parent, int tipoMezzo ) :
    QDialog( parent ), ui( new Ui::AggiungiMezzo ), PTMainController( _mainController )
{

    ui->setupUi( this );

    //Seleziona in automatico il tipo di mezzo
    if ( tipoMezzo != 0 ) {
        ui->comboBox->setCurrentIndex( tipoMezzo );
    }

    //Seleziona in automatico la la data-ora attuale
    ui->dateTimeEdit->setDateTime( QDateTime::currentDateTime() );

    //Sleziona in automatico a dimensione standard per camion
    ui->spinBox->setValue( Camion::GetDimensioniStandardCamion() );

}

AggiungiMezzo::~AggiungiMezzo()
{
    delete ui;
}

void AggiungiMezzo::on_pushButton_aggiungi_clicked() try
{
    switch ( ui->comboBox->currentIndex() ) {
        case 0:
            PTMainController->AggiungiCamion( ui->lineEditTarga->text(), ui->dateTimeEdit->dateTime(), ui->spinBox->value() );
            break;

        case 1:
            PTMainController->AggiungiCamper( ui->lineEditTarga->text(), ui->dateTimeEdit->dateTime() );
            break;

        case 2:
            PTMainController->AggiungiMacchina( ui->lineEditTarga->text(), ui->dateTimeEdit->dateTime() );
            break;

        case 3:
            PTMainController->AggiungiMoto( ui->lineEditTarga->text(), ui->dateTimeEdit->dateTime() );
            break;

        default:
            qDebug() << "comboBox selezione assurda: " << ui->comboBox->currentIndex();
            break;
    }

    close();

} catch ( const EXCEPTIONS &e )
{
    if ( e == exception_PostiEsauriti ) {
        MessageDialog errorePosti( "Posti esauriti", "Non sono disponibili posti a sufficienza per effettuare l'operazione" );
        errorePosti.exec();
    } else if ( e == exception_TargaNonValida ) {
        MessageDialog erroreTarga( "Targa non adatta", "Il campo targa deve contenere una targa valida." );
        erroreTarga.exec();
    } else {
        throw;
    }
}

void AggiungiMezzo::on_comboBox_currentIndexChanged( const QString &arg1 )
{
    if ( arg1 == "Camion" ) {
        ui->spinBox->setEnabled( true );
    } else {
        ui->spinBox->setEnabled( false );
    }
}

void AggiungiMezzo::on_pushButton_annulla_clicked()
{
    close();
}

