#ifndef MACCHINA_H
#define MACCHINA_H

#include "automezzo.h"

class Macchina : public AutoMezzo
{
    static double costoPerOra;

    public:
        Macchina( const QString &_targa );
        virtual ~Macchina();

        double virtual GetCostoServizioPerOra() const;

        static double GetCostoPerOra();
        static double SetCostoPerOra( const double &_CostoPerOra );
};

#endif // MACCHINA_H
