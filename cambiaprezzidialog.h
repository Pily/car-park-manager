#ifndef CAMBIAPREZZIDIALOG_H
#define CAMBIAPREZZIDIALOG_H

#include <QDialog>
#include "controller.h"

namespace Ui {
class cambiaPrezziDialog;
}

class cambiaPrezziDialog : public QDialog
{
    Q_OBJECT

public:
    explicit cambiaPrezziDialog(Controller * const _PTMainController, QWidget *parent = 0);
    ~cambiaPrezziDialog();

private slots:
    void on_pushButton_ok_clicked();

    void on_pushButton_annulla_clicked();

private:
    Ui::cambiaPrezziDialog *ui;
    Controller * const PTMainController;
};

#endif // CAMBIAPREZZIDIALOG_H
