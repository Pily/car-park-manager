#include "moto.h"

double Moto::costoPerOra = 0.75;

Moto::Moto( const QString &_targa ) : AutoMezzo( _targa ) {}

Moto::~Moto() {}

double Moto::GetCostoServizioPerOra() const
{
    return costoPerOra;
}

double Moto::GetCostoPerOra()
{
    return costoPerOra;
}

double Moto::SetCostoPerOra( const double &_CostoPerOra ) try
{
    if ( _CostoPerOra <= 0 ) {
        throw ( exception_CostoPerOra_absurd );
    }

    return costoPerOra = _CostoPerOra;

} catch ( const Exceptions &e )
{
    if ( e == exception_CostoPerOra_absurd ) {
        qDebug() << "Costo per ora assurdo: " << _CostoPerOra;
    }

    return costoPerOra;
}


