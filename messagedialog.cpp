#include "messagedialog.h"
#include "ui_messagedialog.h"

MessageDialog::MessageDialog(const QString & _title, const QString & _text, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MessageDialog)
{
    ui->setupUi(this);
    setWindowTitle(_title);
    ui->textBrowser->setText(_text);
}

MessageDialog::~MessageDialog()
{
    delete ui;
}

void MessageDialog::on_pushButton_back_clicked()
{
    close();
}
