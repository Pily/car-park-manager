#-------------------------------------------------
#
# Project created by QtCreator 2015-12-31T16:12:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Car-Park-Manager
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    automezzo.cpp \
    camion.cpp \
    camper.cpp \
    macchina.cpp \
    moto.cpp \
    mezzoparcheggiato.cpp \
    controller.cpp \
    aggiungimezzo.cpp \
    cambiaposti.cpp \
    aboutdialog.cpp \
    messagedialog.cpp \
    pagamentodialog.cpp \
    eliminazionedialog.cpp \
    cambiaprezzidialog.cpp \
    resetdialog.cpp

HEADERS  += mainwindow.h \
    automezzo.h \
    camion.h \
    camper.h \
    macchina.h \
    mezzi.h \
    moto.h \
    mezzoparcheggiato.h \
    controller.h \
    dlist.h \
    aggiungimezzo.h \
    cambiaposti.h \
    aboutdialog.h \
    excepitons.h \
    messagedialog.h \
    pagamentodialog.h \
    eliminazionedialog.h \
    cambiaprezzidialog.h \
    resetdialog.h

FORMS    += mainwindow.ui \
    aggiungimezzo.ui \
    cambiaposti.ui \
    aboutdialog.ui \
    messagedialog.ui \
    pagamentodialog.ui \
    eliminazionedialog.ui \
    cambiaprezzidialog.ui \
    resetdialog.ui

DISTFILES +=

RESOURCES += \
    resources.qrc
