#include "cambiaposti.h"
#include "ui_cambiaposti.h"
#include "controller.h"
#include "mainwindow.h"
#include "messagedialog.h"
#include "excepitons.h"

CambiaPosti::CambiaPosti( Controller *const _mainController, QWidget *parent ) :
    QDialog( parent ), ui( new Ui::CambiaPosti ), PTMainController( _mainController )
{
    ui->setupUi( this );

    ui->spinBox_Camion->setValue( PTMainController->GetSpaziCamion() );
    ui->spinBox_Camper->setValue( PTMainController->GetSpaziCamper() );
    ui->spinBox_Macchine->setValue( PTMainController->GetSpaziMacchine() );
    ui->spinBox_Moto->setValue( PTMainController->GetSpaziMoto() );
}

CambiaPosti::~CambiaPosti()
{
    delete ui;
}

void CambiaPosti::on_pushButton_annulla_clicked()
{
    close();
}

void CambiaPosti::on_pushButton_ok_clicked() try
{
    PTMainController->SetSpaziCamion( ui->spinBox_Camion->value() );
    PTMainController->SetSpaziCamper( ui->spinBox_Camper->value() );
    PTMainController->SetSpaziMacchine( ui->spinBox_Macchine->value() );
    PTMainController->SetSpaziMoto( ui->spinBox_Moto->value() );
    close();
} catch ( const EXCEPTIONS &e )
{
    if ( e == exception_MezziAncoraInLista ) {
        MessageDialog erroreTarga( "Spazi troppo ridotti", "Il numero di spazi selezionato troppo ridotto per contenere i mezzi ancora parcheggiati." );
        erroreTarga.exec();
    } else {
        throw;
    }
}
