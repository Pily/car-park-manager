#include "eliminazionedialog.h"
#include "ui_eliminazionedialog.h"

EliminazioneDialog::EliminazioneDialog(DLista<MezzoParcheggiato>::Iterator _iter, Controller * const _PTMainController, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::eliminazioneDialog),
    iter(_iter),
    PTMainController(_PTMainController)
{
    ui->setupUi(this);
}

EliminazioneDialog::~EliminazioneDialog()
{
    delete ui;
}

void EliminazioneDialog::on_pushButton_ok_clicked()
{
    PTMainController->TogliMezzo( iter );
    close();
}

void EliminazioneDialog::on_pushButton_cancel_clicked()
{
    close();
}
