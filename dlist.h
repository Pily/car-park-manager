#ifndef DLIST_H
#define DLIST_H

#include <QDebug>

template <class T>
class DLista
{

        class Nodo;
        class SmartPointer;

        class Nodo
        {
                friend class DLista;
                friend class Iterator;

                T info;
                int riferimenti;
                SmartPointer prev;
                SmartPointer next;
            public:
                Nodo() :
                    riferimenti( 0 )
                {
                }
                Nodo( const T &_info ) :
                    info( _info ), riferimenti( 0 ), prev( 0 ), next( 0 )
                {
                }

        };

        class SmartPointer
        {
            public:
                Nodo *punt;

                //Funziona nache da convertitore implicito da Nodo* a Smartpointer
                SmartPointer( Nodo *_punt = 0 ) :
                    punt( _punt )
                {
                    if ( punt ) {
                        punt->riferimenti++;
                    }
                }

                //Costruttore di copia
                SmartPointer( const SmartPointer &_SPt ) :
                    punt( _SPt.punt )
                {
                    if ( punt ) {
                        punt->riferimenti++;
                    }
                }

                //Distruttore
                ~SmartPointer()
                {
                    if ( punt ) {
                        punt->riferimenti--;

                        if ( punt->riferimenti < 1 ) {
                            delete punt;
                        }
                    }
                }

                SmartPointer &operator=( const SmartPointer &_SPt )
                {
                    if ( this != &_SPt ) {

                        Nodo *t = punt;
                        //Aggiungo 1 a punt
                        //Tolgo 1 a punt
                        punt = _SPt.punt;

                        if ( punt ) {
                            punt->riferimenti++;
                        }

                        if ( t ) {
                            t->riferimenti--;

                            if ( t->riferimenti < 1 ) {
                                delete t;
                            }
                        }

                    }

                    return *this;
                }

                Nodo &operator*() const
                {
                    return *punt;
                }
                Nodo *operator->() const
                {
                    return punt;
                }

                bool operator==( const SmartPointer &x ) const
                {
                    return punt == x.punt;
                }
                bool operator!=( const SmartPointer &x ) const
                {
                    return punt != x.punt;
                }

        };

        SmartPointer first;
        SmartPointer last;

        T Remove( const SmartPointer &_SPt ) try
        {
            if ( _SPt == 0 ) {
                throw ( exception_NodoAssente );
            }

            T aux = _SPt->info;

            if ( _SPt == first ) {
                first = first->next;
            } else {
                SmartPointer backPt = first;

                while ( backPt != 0 && backPt->next != _SPt ) { //backpt->next
                    backPt = backPt->next;
                }

                if ( backPt != 0 ) {
                    backPt->next = _SPt->next;
                }
            }

            return aux;

        } catch ( const Exceptions &e )
        {
            if ( e == exception_NodoAssente ) {
                qDebug() << "Nodo assente";
            }

            throw;
        }

        //Add potrebbe rompere l'ordine dei nodi
        /*T Add(const T & _info, const SmartPointer & _SPt) {
            if (_SPt) {
                if (_SPt == first) {
                    first = new Nodo(_info, first);
                } else {
                    _SPt->next = new Nodo(_info, _SPt->next);
                }
                return _info;
            }
        }*/

    public:

        enum Exceptions {
            exception_ListaVuota,
            exception_NodoAssente,
            exception_NodoNonTrovato,
            exception_EndOfList
        };

        class Iterator
        {
                SmartPointer punt;

            public:

                Iterator( const SmartPointer &_punt ) :
                    punt( _punt )
                {
                }

                Iterator &operator++() try
                {
                    if ( punt == 0 ) {
                        throw ( exception_EndOfList );
                    }

                    punt = punt->next;
                    return *this;

                } catch ( const Exceptions &e )
                {
                    if ( e == exception_EndOfList ) {
                        qDebug() << "Iteratore a fine lista";
                    }

                    return *this;
                }

                Iterator operator++( int ) try
                {
                    if ( punt == 0 ) {
                        throw ( exception_EndOfList );
                    }

                    punt = punt->next;
                    Iterator aux = *this;
                    return aux;
                } catch ( const Exceptions &e )
                {
                    if ( e == exception_EndOfList ) {
                        qDebug() << "Iteratore a fine lista";
                    }

                    return *this;
                }

                Iterator &operator--() try
                {
                    if ( punt == 0 ) {
                        throw ( exception_EndOfList );
                    }

                    punt = punt->prev;
                    return *this;

                } catch ( const Exceptions &e )
                {
                    if ( e == exception_EndOfList ) {
                        qDebug() << "Iteratore a fine lista";
                    }

                    return *this;
                }

                Iterator operator--( int ) try
                {
                    if ( punt == 0 ) {
                        throw ( exception_EndOfList );
                    }

                    punt = punt->prev;
                    Iterator aux = *this;
                    return aux;
                } catch ( const Exceptions &e )
                {
                    if ( e == exception_EndOfList ) {
                        qDebug() << "Iteratore a fine lista";
                    }

                    return *this;
                }

                bool operator==( const Iterator &x ) const
                {
                    return punt == x.punt;
                }
                bool operator!=( const Iterator &x ) const
                {
                    return punt != x.punt;
                }

                T &operator*()
                {
                    return punt->info;
                }

                bool Valido() const
                {
                    return punt != 0;
                }

                T Extract() try
                {

                    if ( punt == 0 ) {
                        throw ( exception_NodoAssente );
                    }

                    T aux = punt->info;

                    if ( punt->prev != 0 ) {
                        punt->prev->next = punt->next;
                    }

                    if ( punt->next != 0 ) {
                        punt->next->prev = punt->prev;
                    }

                    punt->prev = 0;
                    punt->next = 0;

                    return aux;

                } catch ( const Exceptions &e )
                {
                    if ( e == exception_NodoAssente ) {
                        qDebug() << "Nodo assente";
                    }

                    throw;
                }
        };

        DLista() :
            first( 0 ), last( 0 )
        {
        }
        DLista( const DLista &_list ) :
            first( _list.first ), last( _list.last )
        {
        }

        Iterator Begin() const
        {
            return Iterator( first );
        }
        Iterator End() const
        {
            return Iterator( last );
        }

        bool Empty() const
        {
            return first == SmartPointer( 0 ) && last == SmartPointer( 0 );
        }

        T Push( const T &_info )
        {
            if ( Empty() ) {
                first = last = new Nodo( _info );
            } else {
                if ( _info < first->info ) {
                    SmartPointer PtExFirst = first;
                    first = new Nodo( _info );
                    first->next = PtExFirst;
                    PtExFirst->prev = first;
                } else if ( _info > last->info || _info == last->info ) {
                    SmartPointer PtExLast = last;
                    last = new Nodo( _info );
                    last->prev = PtExLast;
                    PtExLast->next = last;
                } else {
                    SmartPointer resSPT = first;

                    while ( resSPT->next != 0 && _info > resSPT->info ) {
                        resSPT = resSPT->next;
                    }

                    SmartPointer PtPrev = resSPT->prev;
                    SmartPointer PtNewNodo = new Nodo( _info );
                    PtNewNodo->prev = PtPrev;
                    PtNewNodo->next = resSPT;
                    PtPrev->next = PtNewNodo;
                    resSPT->prev = PtNewNodo;
                }

            }

            return _info;
        }

        T Pop( const T &_info ) try
        {
            if ( Empty() ) {
                throw ( exception_ListaVuota );
            } else if ( first == last ) {
                if ( _info == first->info ) {
                    return Pop_Begin();
                } else {
                    throw ( exception_NodoNonTrovato );
                }
            } else if ( _info == first->info ) {
                return Pop_Begin();
            } else if ( _info == last->info ) {
                return Pop_End();
            } else {
                SmartPointer resSPT = first;

                while ( resSPT != 0 && resSPT->info != _info ) {
                    resSPT = resSPT->next;
                }

                if ( resSPT != 0 ) { //allora _info == resSPT->info
                    resSPT->prev->next = resSPT->next;
                    resSPT->next->prev = resSPT->prev;
                    return resSPT->info;
                } else {
                    throw ( exception_NodoNonTrovato );
                }
            }

        } catch ( const Exceptions &e )
        {
            switch ( e ) {
                case exception_ListaVuota:
                    qDebug() << "Lista vuota";
                    throw;

                    break;

                case exception_NodoNonTrovato:
                    qDebug() << "Nodo non trovato";
                    throw;

                default:
                    throw;
                    break;
            }
        }

        T Pop( Iterator _iter ) try
        {
            if ( Empty() ) {
                throw ( exception_ListaVuota );
            } else if ( first == last ) {
                if ( _iter.Valido() ) {
                    return Pop_Begin();
                } else {
                    throw ( exception_NodoNonTrovato );
                }
            } else if ( _iter == Begin() ) {
                return Pop_Begin();
            } else if ( _iter == End() ) {
                return Pop_End();
            } else {
                if ( _iter.Valido() ) {
                    return _iter.Extract();
                } else {
                    throw ( exception_NodoNonTrovato );
                }
            }

        } catch ( const Exceptions &e )
        {
            switch ( e ) {
                case exception_ListaVuota:
                    qDebug() << "Lista vuota";
                    throw;

                    break;

                case exception_NodoNonTrovato:
                    qDebug() << "Nodo non trovato";
                    throw;

                default:
                    throw;
                    break;
            }
        }


        T Pop_Begin() try
        {
            if ( first == 0 ) { //Lista vuota
                throw ( exception_ListaVuota );
            }

            if ( first == last ) {
                Iterator it = Begin();
                first = last = 0;
                return it.Extract();
            } else {
                Iterator it = Begin();
                first = first->next;
                return it.Extract();
            }
        } catch ( const Exceptions &e )
        {
            if ( e == exception_ListaVuota ) {
                qDebug() << "Lista vuota";
                throw;
            }

            throw;
        }

        T Pop_End() try
        {
            if ( last == 0 ) { //Lista vuota
                throw ( exception_ListaVuota );
            }

            if ( first == last ) {
                Iterator it = End();
                first = last = 0;
                return it.Extract();
            } else {
                Iterator it = End();
                last = last->prev;
                return it.Extract();
            }
        } catch ( const Exceptions &e )
        {
            if ( e == exception_ListaVuota ) {
                qDebug() << "Lista vuota";
                throw;
            }

            throw;
        }

        T operator[](unsigned int _index){
            Iterator iter = first;
            for (int var = 0; var < _index; ++var) {
                ++iter;
            }
            return *iter;
        }

        unsigned int Size() const
        {
            SmartPointer SPt = first;
            unsigned int cont = 0;

            while ( SPt != 0 ) {
                cont++;
                SPt = SPt->next;
            }

            return cont;
        }

        void Flush()
        {
            first = 0;
            last = 0;
        }

        QList<T> ToQList() const
        {
            QList<T> aux;
            DLista<T>::Iterator iter = Begin();

            while ( iter.Valido() ) {
                aux.append( *iter );
                ++iter;
            }

            return aux;
        }

};


#endif // DLIST_H
