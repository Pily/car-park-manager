#include "automezzo.h"

AutoMezzo::AutoMezzo( const QString &_targa ) : targa( _targa ) {}

AutoMezzo::~AutoMezzo() {}

unsigned int AutoMezzo::GetPostiOccupati() const
{
    return 1;
}

QString AutoMezzo::GetTarga() const
{
    return targa;
}

bool AutoMezzo::operator< ( const AutoMezzo &x ) const
{
    return targa < x.targa;
}

bool AutoMezzo::operator> ( const AutoMezzo &x ) const
{
    return targa > x.targa;
}

bool AutoMezzo::operator!= ( const AutoMezzo &x ) const
{
    return targa != x.targa;
}

bool AutoMezzo::operator== ( const AutoMezzo &x ) const
{
    return targa == x.targa;
}

QDebug operator <<( QDebug deb, AutoMezzo *const x )
{
    deb.nospace() << "TARGA: " << x->targa;
    deb.space();
    deb.nospace() << "COSTO/h: " << x->GetCostoServizioPerOra();
    deb.space();
    deb.nospace() << "POSTI: " << x->GetPostiOccupati();
    return deb.space();
}
