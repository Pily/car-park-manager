#ifndef CAMPER_H
#define CAMPER_H

#include "automezzo.h"

class Camper : public AutoMezzo
{
    static double costoPerOra;

    public:
        Camper( const QString &_targa );
        virtual ~Camper();

        double virtual GetCostoServizioPerOra() const;

        static double GetCostoPerOra();
        static double SetCostoPerOra( const double &_CostoPerOra );
};

#endif // CAMPER_H
