#include "mezzoparcheggiato.h"

MezzoParcheggiato::MezzoParcheggiato( AutoMezzo *const _mezzo, const QDateTime &_arrivo ) :
    mezzo( _mezzo ), arrivo( _arrivo ) {}

QString MezzoParcheggiato::GetTarga() const
{
    return mezzo->GetTarga();
}

double MezzoParcheggiato::GetCostoPerOra() const
{
    return mezzo->GetCostoServizioPerOra();
}

unsigned int MezzoParcheggiato::GetPostiOccupati() const
{
    return mezzo->GetPostiOccupati();
}

QDateTime MezzoParcheggiato::GetArrivo() const
{
    return arrivo;
}

double MezzoParcheggiato::GetCostoServizio( const QDateTime &_uscita )
{
    return mezzo->GetCostoServizioPerOra() * static_cast<double>( arrivo.secsTo( _uscita ) ) / 3600;
}

AutoMezzo *MezzoParcheggiato::GetMezzo() const
{
    return mezzo;
}

bool MezzoParcheggiato::operator !=( const MezzoParcheggiato &x ) const
{
    return *mezzo != *(x.mezzo) || arrivo != x.arrivo;
}

bool MezzoParcheggiato::operator ==( const MezzoParcheggiato &x ) const
{
    return *mezzo == *(x.mezzo) && arrivo == x.arrivo;
}

bool MezzoParcheggiato::operator <( const MezzoParcheggiato &x ) const
{
    return *mezzo < *(x.mezzo);
}

bool MezzoParcheggiato::operator >( const MezzoParcheggiato &x ) const
{
    return *(mezzo) > *(x.mezzo);
}
