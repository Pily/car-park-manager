#ifndef AGGIUNGIMEZZO_H
#define AGGIUNGIMEZZO_H

#include <QDialog>
#include "controller.h"
#include "messagedialog.h"

namespace Ui
{
    class AggiungiMezzo;
}

class AggiungiMezzo : public QDialog
{
        Q_OBJECT


    public:
        explicit AggiungiMezzo( Controller *const _mainController, QWidget *parent = 0 , int type = 0 );
        ~AggiungiMezzo();

    private slots:
        void on_pushButton_aggiungi_clicked();

        void on_comboBox_currentIndexChanged( const QString &arg1 );

        void on_pushButton_annulla_clicked();

    private:
        Ui::AggiungiMezzo *ui;
        Controller *const PTMainController;
};

#endif // AGGIUNGIMEZZO_H
