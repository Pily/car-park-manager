#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "dlist.h"
#include "mezzoparcheggiato.h"
#include "excepitons.h"

#include <QXmlStreamReader>
#include <QXmlStreamWriter>
#include <QFile>
#include <QFileInfo>
#include <QFileDialog>

class Controller
{
        DLista<MezzoParcheggiato> lista_totale;

        //Spazi disponibili
        unsigned int spaziCamionTotali;
        unsigned int spaziCamperTotali;
        unsigned int spaziMacchineTotali;
        unsigned int spaziMotoTotali;

        MezzoParcheggiato EstraiMezzo( DLista<MezzoParcheggiato>::Iterator _iter );
        bool fileDefault();

    public:

        Controller();

        QStringList ToQStringList() const;

        void Reset();

        bool Load( const QString &_nomeFile = "" );
        bool Save( const QString &_nomeFile = "" ) const;

        void AggiungiMacchina( const QString &_targa, const QDateTime &_arrivo );
        void AggiungiMoto( const QString &_targa, const QDateTime &_arrivo );
        void AggiungiCamion( const QString &_targa, const QDateTime &_arrivo,
                             const unsigned int &_postiOccupati = 1 );
        void AggiungiCamper( const QString &_targa, const QDateTime &_arrivo );

        void TogliMezzo( const DLista<MezzoParcheggiato>::Iterator _iter );
        double PagaServizio( const DLista<MezzoParcheggiato>::Iterator _iter, const QDateTime _uscita );

        unsigned int GetSpaziOccupatiTotali() const;
        unsigned int GetSpaziCamionOccupati() const;
        unsigned int GetSpaziCamperOccupati() const;
        unsigned int GetSpaziMacchineOccupati() const;
        unsigned int GetSpaziMotoOccupati() const;

        double GetCostoCamion() const;
        double GetCostoCamper() const;
        double GetCostoMacchine() const;
        double GetCostoMoto() const;

        double SetCostoCamion(const double & value );
        double SetCostoCamper(const double & value);
        double SetCostoMacchina(const double & value);
        double SetCostoMoto(const double & value);

        unsigned int GetDimensioneStandardCamion() const;
        unsigned int SetDimensioneStandardCamion(const unsigned int &value);

        template <class T>
        DLista<MezzoParcheggiato> FiltraLista() const
        {
            DLista<MezzoParcheggiato>::Iterator iter = lista_totale.Begin();
            DLista<MezzoParcheggiato> aux;

            while ( iter.Valido() ) {
                if ( dynamic_cast<T *>( ( *iter ).GetMezzo() ) != 0 ) {
                    aux.Push( *iter );
                    ++iter;
                }
            }

            return aux;
        }

        unsigned int GetSpaziLiberiTotali() const;
        unsigned int GetSpaziCamionLiberi() const;
        unsigned int GetSpaziCamperLiberi() const;
        unsigned int GetSpaziMacchineLiberi() const;
        unsigned int GetSpaziMotoLiberi() const;

        void SetSpaziCamper( const unsigned int &_spazi );
        void SetSpaziCamion( const unsigned int &_spazi );
        void SetSpaziMacchine( const unsigned int &_spazi );
        void SetSpaziMoto( const unsigned int &_spazi );

        unsigned int GetSpaziTotali() const;
        unsigned int GetSpaziCamper() const;
        unsigned int GetSpaziCamion() const;
        unsigned int GetSpaziMacchine() const;
        unsigned int GetSpaziMoto() const;

        DLista<MezzoParcheggiato>::Iterator CercaPerIndice( const int &_ind ) const;
        DLista<MezzoParcheggiato>::Iterator CercaPerTarga( const QString &_targa ) const;
};

#endif // CONTROLLER_H
