#ifndef MOTO_H
#define MOTO_H

#include "automezzo.h"

class Moto : public AutoMezzo
{
    static double costoPerOra;

    public:
        Moto( const QString &_targa );
        virtual ~Moto();

        double virtual GetCostoServizioPerOra() const;

        static double GetCostoPerOra();
        static double SetCostoPerOra( const double &_CostoPerOra );
};

#endif // MOTO_H
