#ifndef CAMION_H
#define CAMION_H

#include "automezzo.h"

class Camion : public AutoMezzo
{
        unsigned int postiOccupati;
        static double costoPerOra;
        static unsigned int dimensioniStandardCamion;

    public:
        Camion( const QString &_targa, const int &_postiOccupati = dimensioniStandardCamion );
        virtual ~Camion();

        virtual int SetPostiOccupati( const int &_postiOccupati );

        virtual unsigned int GetPostiOccupati() const;
        virtual double GetCostoServizioPerOra() const;

        static unsigned int GetDimensioniStandardCamion();
        static unsigned int SetDimensioniStandardCamion(unsigned int _dim);

        static double GetCostoPerOra();
        static double SetCostoPerOra( const double &_CostoPerOra );
};

#endif // CAMION_H
