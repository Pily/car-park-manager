﻿#include "controller.h"

Controller::Controller():
    spaziCamionTotali( 20 ),
    spaziCamperTotali( 15 ),
    spaziMacchineTotali( 50 ),
    spaziMotoTotali( 20 )
{
    //TEST da rimuovere
    /*AggiungiCamper( "TEST1", QDateTime::currentDateTime() );
    AggiungiCamper( "TEST2", QDateTime::currentDateTime() );
    AggiungiCamper( "TEST3", QDateTime::currentDateTime() );
    AggiungiCamper( "TEST4", QDateTime::currentDateTime() );*/

    fileDefault();
}

QStringList Controller::ToQStringList() const
{
    QStringList list;
    DLista<MezzoParcheggiato>::Iterator iter = lista_totale.Begin();

    while ( iter.Valido() ) {
        list.append( ( *iter ).GetTarga() );
        ++iter;
    }

    return list;
}

void Controller::Reset()
{
    lista_totale.Flush();
    Load( "default_config.dat" );
}

//FILE

bool Controller::fileDefault()try
{
    return Load( "default_config.dat" );
} catch ( const EXCEPTIONS &e )
{
    if ( e == exception_FileNonTrovato ) {
        return Save( "default_config.dat" );
    } else {
        throw;
    }
}


bool Controller::Load( const QString &_nomeFile )
{
    QString nomeFile = _nomeFile;
    QFileInfo fileInfo( nomeFile );

    if ( nomeFile.size() == 0 ) {
        throw exception_ValoreNonValido;
    } else if ( !fileInfo.exists() ) {
        throw exception_FileNonTrovato;
    }

    QFile file( nomeFile );

    if ( !file.open( QFile::ReadOnly | QFile::Text ) ) {
        qDebug() << "Error: Cannot read file " << qPrintable( nomeFile )
                 << ": " << qPrintable( file.errorString() );
        return false;

    } else {
        QXmlStreamReader reader;
        reader.setDevice( &file );
        reader.readNextStartElement();

        reader.readNextStartElement();

        while ( !( reader.atEnd() ) ) {

            if ( reader.name() == "costoCamion" ) {
                SetCostoCamion( reader.readElementText().toDouble() );
                reader.readNextStartElement();
            } else if ( reader.name() == "costoCamper" ) {
                SetCostoCamper( reader.readElementText().toDouble() );
                reader.readNextStartElement();
            } else if ( reader.name() == "costoMacchina" ) {
                SetCostoMacchina( reader.readElementText().toDouble() );
                reader.readNextStartElement();
            } else if ( reader.name() == "costoMoto" ) {
                SetCostoMoto( reader.readElementText().toDouble() );
                reader.readNextStartElement();
            }

            else if ( reader.name() == "postiCamion" ) {
                SetSpaziCamion( reader.readElementText().toInt() );
                reader.readNextStartElement();
            } else if ( reader.name() == "postiCamper" ) {
                SetSpaziCamper( reader.readElementText().toInt() );
                reader.readNextStartElement();
            } else if ( reader.name() == "postiMacchina" ) {
                SetSpaziMacchine( reader.readElementText().toInt() );
                reader.readNextStartElement();
            } else if ( reader.name() == "postiMoto" ) {
                SetSpaziMoto( reader.readElementText().toInt() );
                reader.readNextStartElement();
            }

            else if ( reader.name() == "dimensioneCamionStandard" ) {
                SetDimensioneStandardCamion( reader.readElementText().toInt() );
                reader.readNextStartElement();
            }

            else if ( reader.name() == "Mezzo" ) {
                reader.readNextStartElement();
                QString tipo = reader.readElementText();
                reader.readNextStartElement();
                QString targa = reader.readElementText();
                reader.readNextStartElement();
                QDateTime arrivo = QDateTime::fromString( reader.readElementText() );
                reader.readNextStartElement();
                unsigned int spazi = reader.readElementText().toInt();

                if ( tipo == "Camion" ) {
                    AggiungiCamion( targa, arrivo, spazi );
                } else if ( tipo == "Camper" ) {
                    AggiungiCamper( targa, arrivo );
                } else if ( tipo == "Macchina" ) {
                    AggiungiMacchina( targa, arrivo );
                } else if ( tipo == "Moto" ) {
                    AggiungiMoto( targa, arrivo );
                }

                reader.readNextStartElement();
                reader.readNextStartElement();
            }

            else {
                reader.readNextStartElement();
            }

        }

        return true;
    }
}

bool Controller::Save( const QString &_nomeFile ) const
{
    QString nomeFile = _nomeFile;

    if ( nomeFile.size() == 0 ) {

        throw exception_ValoreNonValido;
    }

    QFile savefile( _nomeFile );
    savefile.open( QIODevice::WriteOnly );

    QXmlStreamWriter xmlWriter( &savefile );
    xmlWriter.setAutoFormatting( true );

    //CONFIG formattazione
    xmlWriter.setAutoFormattingIndent( -1 );
    xmlWriter.writeStartDocument();

    xmlWriter.writeStartElement( "DATA" );

    xmlWriter.writeComment( "Impostazioni" );
    xmlWriter.writeTextElement( "costoCamion", QString::number( Camion::GetCostoPerOra() ) );
    xmlWriter.writeTextElement( "costoCamper", QString::number( Camper::GetCostoPerOra() ) );
    xmlWriter.writeTextElement( "costoMacchina", QString::number( Macchina::GetCostoPerOra() ) );
    xmlWriter.writeTextElement( "costoMoto", QString::number( Moto::GetCostoPerOra() ) );

    xmlWriter.writeTextElement( "postiCamion", QString::number( spaziCamionTotali ) );
    xmlWriter.writeTextElement( "postiCamper", QString::number( spaziCamperTotali ) );
    xmlWriter.writeTextElement( "postiMacchina", QString::number( spaziMacchineTotali ) );
    xmlWriter.writeTextElement( "postiMoto", QString::number( spaziMotoTotali ) );

    xmlWriter.writeTextElement( "dimensioneCamionStandard", QString::number( Camion::GetDimensioniStandardCamion() ) );

    DLista<MezzoParcheggiato>::Iterator iter = lista_totale.Begin();

    while ( iter.Valido() ) {
        QString tipo;
        QString targa;
        QString posti;
        QString arrivo;

        targa = ( *iter ).GetTarga();
        arrivo = ( *iter ).GetArrivo().toString();

        if ( dynamic_cast<Camion *>( ( *iter ).GetMezzo() ) != 0 ) {
            tipo = "Camion";
            posti = QString::number( ( *iter ).GetPostiOccupati() );
        } else {
            posti = QString::number( 1 );

            if ( dynamic_cast<Camper *>( ( *iter ).GetMezzo() ) != 0 ) {
                tipo = "Camper";
            } else if ( dynamic_cast<Macchina *>( ( *iter ).GetMezzo() ) != 0 ) {
                tipo = "Macchina";
            } else if ( dynamic_cast<Moto *>( ( *iter ).GetMezzo() ) != 0 ) {
                tipo = "Moto";
            } else {
                tipo = "UNKNOWN";
            }
        }

        xmlWriter.writeStartElement( "Mezzo" );
        xmlWriter.writeTextElement( "tipo", tipo );
        xmlWriter.writeTextElement( "targa", targa );
        xmlWriter.writeTextElement( "arrivo", arrivo );
        xmlWriter.writeTextElement( "posti", posti );
        xmlWriter.writeEndElement();

        ++iter;
    }

    xmlWriter.writeEndElement();

    xmlWriter.writeEndDocument();

    savefile.close();
    return true;
}

//Get spazi occupati
unsigned int Controller::GetSpaziOccupatiTotali() const
{
    DLista<MezzoParcheggiato>::Iterator iter = lista_totale.Begin();
    unsigned int counter = 0;

    while ( iter.Valido() ) {
        counter += ( *iter ).GetPostiOccupati();
        ++iter;
    }

    return counter;
}

unsigned int Controller::GetSpaziCamionOccupati() const
{
    DLista<MezzoParcheggiato>::Iterator iter = lista_totale.Begin();
    unsigned int counter = 0;

    while ( iter.Valido() ) {
        if ( dynamic_cast<Camion *>( ( *iter ).GetMezzo() ) ) {
            counter += ( *iter ).GetPostiOccupati();
        }

        ++iter;
    }

    return counter;
}

unsigned int Controller::GetSpaziCamperOccupati() const
{
    DLista<MezzoParcheggiato>::Iterator iter = lista_totale.Begin();
    unsigned int counter = 0;

    while ( iter.Valido() ) {
        if ( dynamic_cast<Camper *>( ( *iter ).GetMezzo() ) ) {
            ++counter;
        }

        ++iter;
    }

    return counter;
}

unsigned int Controller::GetSpaziMacchineOccupati() const
{
    DLista<MezzoParcheggiato>::Iterator iter = lista_totale.Begin();
    unsigned int counter = 0;

    while ( iter.Valido() ) {
        if ( dynamic_cast<Macchina *>( ( *iter ).GetMezzo() ) ) {
            ++counter;
        }

        ++iter;
    }

    return counter;
}

unsigned int Controller::GetSpaziMotoOccupati() const
{
    DLista<MezzoParcheggiato>::Iterator iter = lista_totale.Begin();
    unsigned int counter = 0;

    while ( iter.Valido() ) {
        if ( dynamic_cast<Moto *>( ( *iter ).GetMezzo() ) ) {
            ++counter;
        }

        ++iter;
    }

    return counter;
}

double Controller::GetCostoCamion() const
{
    return Camion::GetCostoPerOra();
}

double Controller::GetCostoCamper() const
{
    return Camper::GetCostoPerOra();
}

double Controller::GetCostoMacchine() const
{
    return Macchina::GetCostoPerOra();
}

double Controller::GetCostoMoto() const
{
    return Moto::GetCostoPerOra();
}

double Controller::SetCostoCamion( const double &value )
{
    return Camion::SetCostoPerOra( value );
}

double Controller::SetCostoCamper( const double &value )
{
    return Camper::SetCostoPerOra( value );
}

double Controller::SetCostoMacchina( const double &value )
{
    return Macchina::SetCostoPerOra( value );
}

double Controller::SetCostoMoto( const double &value )
{
    return Moto::SetCostoPerOra( value );
}

unsigned int Controller::GetDimensioneStandardCamion() const
{
    return Camion::GetDimensioniStandardCamion();
}

unsigned int Controller::SetDimensioneStandardCamion( const unsigned int &value )
{
    return Camion::SetDimensioniStandardCamion( value );
}

//Get spazi totali
unsigned int Controller::GetSpaziTotali() const
{
    return spaziCamionTotali + spaziCamperTotali + spaziMacchineTotali + spaziMotoTotali;
}
unsigned int Controller::GetSpaziCamper() const
{
    return spaziCamperTotali;
}
unsigned int Controller::GetSpaziCamion() const
{
    return spaziCamionTotali;
}
unsigned int Controller::GetSpaziMacchine() const
{
    return spaziMacchineTotali;
}
unsigned int Controller::GetSpaziMoto() const
{
    return spaziMotoTotali;
}

//Get spazi liberi
unsigned int Controller::GetSpaziLiberiTotali() const
{
    return GetSpaziTotali() - GetSpaziOccupatiTotali();
}
unsigned int Controller::GetSpaziCamionLiberi() const
{
    return spaziCamionTotali - GetSpaziCamionOccupati();
}
unsigned int Controller::GetSpaziCamperLiberi() const
{
    return spaziCamperTotali - GetSpaziCamperOccupati();
}
unsigned int Controller::GetSpaziMacchineLiberi() const
{
    return spaziMacchineTotali - GetSpaziMacchineOccupati();
}
unsigned int Controller::GetSpaziMotoLiberi() const
{
    return spaziMotoTotali - GetSpaziMotoOccupati();
}

//Set spazi totali
void Controller::SetSpaziCamion( const unsigned int &_spazi )
{
    if ( _spazi < GetSpaziCamionOccupati() ) {
        throw ( exception_MezziAncoraInLista );
    }

    spaziCamionTotali = _spazi;
}
void Controller::SetSpaziCamper( const unsigned int &_spazi )
{
    if ( _spazi < GetSpaziCamperOccupati() ) {
        throw ( exception_MezziAncoraInLista );
    }

    spaziCamperTotali = _spazi;
}
void Controller::SetSpaziMacchine( const unsigned int &_spazi )
{
    if ( _spazi < GetSpaziMacchineOccupati() ) {
        throw ( exception_MezziAncoraInLista );
    }

    spaziMacchineTotali = _spazi;
}
void Controller::SetSpaziMoto( const unsigned int &_spazi )
{
    if ( _spazi < GetSpaziMotoOccupati() ) {
        throw ( exception_MezziAncoraInLista );
    }

    spaziMotoTotali = _spazi;
}


//Ricerca

DLista<MezzoParcheggiato>::Iterator Controller::CercaPerTarga( const QString &_targa ) const try
{
    DLista<MezzoParcheggiato>::Iterator iter = lista_totale.Begin();

    while ( iter.Valido() && ( *iter ).GetTarga() < _targa ) {
        ++iter;
    }

    if ( iter.Valido() ) {
        return iter;
    } else {
        throw ( exception_MezzoNonTrovato );
    }

} catch ( const EXCEPTIONS &e )
{
    if ( e == exception_MezzoNonTrovato ) {
        qDebug() << "Mezzo non trovato";
        return DLista<MezzoParcheggiato>::Iterator( 0 );
    } else {
        throw;
    }
}

DLista<MezzoParcheggiato>::Iterator Controller::CercaPerIndice( const int &_ind ) const try
{
    DLista<MezzoParcheggiato>::Iterator iter = lista_totale.Begin();

    for ( int var = 0; var < _ind; ++var ) {
        if ( iter.Valido() ) {
            ++iter;
        } else {
            throw ( exception_MezzoNonTrovato );
        }
    }

    return iter;
} catch ( const EXCEPTIONS &e )
{
    if ( e == exception_MezzoNonTrovato ) {
        qDebug() << "Mezzo non trovato";
        return DLista<MezzoParcheggiato>::Iterator( 0 );
    } else {
        throw;
    }
}

//Estrai

MezzoParcheggiato Controller::EstraiMezzo( DLista<MezzoParcheggiato>::Iterator _iter )
{
    if ( _iter.Valido() ) {
        return lista_totale.Pop( _iter );
    } else {
        throw ( exception_MezzoNonTrovato );
    }
}

//Operazioni
void Controller::TogliMezzo( const DLista<MezzoParcheggiato>::Iterator _iter )
{
    EstraiMezzo( _iter );
}

double Controller::PagaServizio( const DLista<MezzoParcheggiato>::Iterator _iter, const QDateTime _uscita )
{
    return EstraiMezzo( _iter ).GetCostoServizio( _uscita );
}

//Aggiungi mezzo
void Controller::AggiungiMacchina( const QString &_targa, const QDateTime &_arrivo )
{
    if ( _targa.size() == 0 ) {
        throw ( exception_TargaNonValida );
    }

    if ( GetSpaziMacchineLiberi() > 0 ) {
        lista_totale.Push( MezzoParcheggiato( new Macchina( _targa ), _arrivo ) );
    } else {
        throw ( exception_PostiEsauriti );
    }
}
void Controller::AggiungiMoto( const QString &_targa, const QDateTime &_arrivo )
{
    if ( _targa.size() == 0 ) {
        throw ( exception_TargaNonValida );
    }

    if ( GetSpaziMotoLiberi() > 0 ) {
        lista_totale.Push( MezzoParcheggiato( new Moto( _targa ), _arrivo ) );
    } else {
        throw ( exception_PostiEsauriti );
    }
}
void Controller::AggiungiCamion( const QString &_targa, const QDateTime &_arrivo, const unsigned int &_postiOccupati )
{
    if ( _targa.size() == 0 ) {
        throw ( exception_TargaNonValida );
    }

    if ( GetSpaziCamionLiberi() >= _postiOccupati ) {
        lista_totale.Push( MezzoParcheggiato( new Camion( _targa, _postiOccupati ), _arrivo ) );
    } else {
        throw ( exception_PostiEsauriti );
    }
}
void Controller::AggiungiCamper( const QString &_targa, const QDateTime &_arrivo )
{
    if ( _targa.size() == 0 ) {
        throw ( exception_TargaNonValida );
    }

    if ( GetSpaziCamperLiberi() > 0 ) {
        lista_totale.Push( MezzoParcheggiato( new Camper( _targa ), _arrivo ) );
    } else {
        throw ( exception_PostiEsauriti );
    }
}






